local modpath = minetest.get_modpath(minetest.get_current_modname())

dofile(modpath .. "/core.lua")

if minetest.get_modpath("default") then
	dofile(modpath .. "/examples.lua")
end
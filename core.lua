fake_liquids = {}

fake_liquids.register = function (base, new_name)
	local liquid_name = base:gmatch(".*:(.*)")()
	
	local realdef = minetest.registered_nodes[base]
	if not realdef then return end
	
	local newdef = {}
	
	newdef.description = "Fake " .. realdef.description
	
	newdef.tiles = realdef.tiles
	newdef.special_tiles = realdef.special_tiles
	
	newdef.alpha = realdef.alpha
	newdef.paramtype = "light"
	
	newdef.light_source = realdef.light_source
	
	newdef.walkable = false
	newdef.climbable = true
	newdef.pointable = false
	newdef.diggable = false
	newdef.buildable_to = true
	newdef.is_ground_content = false
	
	newdef.drawtype = "liquid"
	newdef.post_effect_color = realdef.post_effect_color
	
	minetest.register_node(new_name or "fake_liquids:" .. liquid_name, newdef)
	
	return new_name or "fake_liquids:" .. liquid_name
end



--[[ minetest.register_node("fake_liquids:water", {
	description = "Fake Water",
	drawtype = "glasslike",
	tiles = {
		{
			name = "default_water_source_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
		},
	},
	special_tiles = {
		-- New-style water source material (mostly unused)
		{
			name = "default_water_source_animated.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 2.0,
			},
			backface_culling = false,
		},
	},
	alpha = 160,
	paramtype = "light",
	walkable = false,
	climbable = true,
	pointable = false,
	diggable = false,
	buildable_to = true,
	is_ground_content = false,
}) ]]
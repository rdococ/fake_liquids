# fake_liquids

The fake_liquids mod for Minetest adds fake liquids. These are nodes that look like water, and are swimmable, but do not flow.

To register a fake liquid for your mod, you must first include fake_liquids as a dependency. Once you have done that, you should call the fake_liquids.register function with the name of the liquid you want a fake version of. Upon success, the function will return the name of the new node. You may specify the name of the new node as a second argument if necessary.

If you need to see examples, take a look at examples.lua. That file is what creates the fake water, river water, and fake lava from the liquids in minetest_game.